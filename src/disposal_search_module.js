import $ from 'jquery';
import 'jquery-ui/ui/widgets/autocomplete';

function tranformData(data){
  let transformedData = [];
  if (Array.isArray(data) && data.length){
    transformedData = data.map(function(value, index){
      return {
        label: value.item,
        value: value.item,
        what_to_do: value.what_to_do,
        handle_as: value.category,
        more_info: value.more_info
      }
    });
  } 
  return transformedData;
}

function recyclingTitle(recyclingObj){
  const html = `<div class='paragraph-content'>
                  <h3 class='title'>${recyclingObj.label}</h3>
                </div>`;
  return html;
}

function recyclingHandleAsSpan(handle_as){
  const html = handle_as !== "" ? `<span>${handle_as}</span>` : "";
  return html;
}

function recyclingMoreInfoLi(more_info){
  const html = more_info !== "" ? `<li>${more_info}</li>` : "";
  return html;
}

function recyclingWhatToDoLi(what_to_do){
  return `<li>${what_to_do}</li>`; 
}

function recyclingInfoContent(recyclingObj){
  const html = `<div class='cell medium-12 large-8'>
                  <div class='paragraph-content'>
                    ${recyclingHandleAsSpan(recyclingObj.handle_as)}
                    <ul>
                      ${recyclingWhatToDoLi(recyclingObj.what_to_do)}
                      ${recyclingMoreInfoLi(recyclingObj.more_info)}
                    </ul>
                  </div>
                </div>`;
  return html;
}


function clearPreviousResults() {
  const sections = document.querySelectorAll(".organism.organism--scannable-paragraphs.organism--collapsed.recycling");
  for(let i = 0; i < sections.length; i++){
    const section = sections[i];
    section.parentNode.removeChild(section);
  }
}

function updateResultsHeader(num) {
  const resultTotalElem = document.getElementById("recycling-results-total");
  const resultNameElem = document.getElementById("recycling-results-name");

  if(isNaN(num)){
    resultTotalElem.innerText = "0";
    resultNameElem.innerText = "results";
  }else{
    resultTotalElem.innerText = num;
    resultNameElem.innerText = num === 1 ? "result" : "results";
  }
}

function buildRecyclingResult(recyclingObj){
  const sectionNode = document.createElement("section");
  sectionNode.classList.add("organism");
  sectionNode.classList.add("organism--scannable-paragraphs")
  sectionNode.classList.add("organism--collapsed");
  sectionNode.classList.add("recycling");

  const sectionHtml = `<div class='grid-container fluid'>
                        <div class='grid-x grid-margin-x'>
                          <div class='cell'>
                            <div class='molecule molecule--scannable-paragraph'>
                              <div class='grid-x grid-margin-x medium-margin-collapse'>
                                <div class='cell medium-12 large-4'>
                                  ${recyclingTitle(recyclingObj)}
                                </div>
                                ${recyclingInfoContent(recyclingObj)}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>`;

  sectionNode.innerHTML = sectionHtml;
  return sectionNode;
}

function showRecyclingResult(recyclingObj) {
  const recyclingheader = document.getElementById("recycling-search-results-header");
  const sectionNode = buildRecyclingResult(recyclingObj);

  clearPreviousResults();
  updateResultsHeader(1);
  recyclingheader.parentNode.insertBefore(sectionNode, recyclingheader.nextSibling);
}

function showRecyclingResults(recyclingObjArr){
  const recyclingheader = document.getElementById("recycling-search-results-header");
  clearPreviousResults();
  
  const fragment = document.createDocumentFragment();
  recyclingObjArr.forEach(function(recyclingObj){
    const sectionNode = buildRecyclingResult(recyclingObj);
    fragment.appendChild(sectionNode);
  });

  updateResultsHeader(recyclingObjArr.length);
  recyclingheader.parentNode.insertBefore(fragment, recyclingheader.nextSibling); 
}

function filterArray(recyclingArr, term){
  let arr = [];
  if(recyclingArr.length && term !== ""){
    arr = $.ui.autocomplete.filter(recyclingArr, term);
  }
  return arr;
}

const disposalSearchModule = {
  addCSSHelper: function(url){
    const head = document.head;
    const link = document.createElement("link");
    link.rel = "stylesheet";
    link.type = "text/css";
    link.href = url;
    head.appendChild(link);
  },
  init: function(url){
    const ajaxUrl = url;
    let recyclingItems = [];

    $("#recycling-wizard").autocomplete({
      open: function(event, ui){
        const wizardInputWidth = event.target.offsetWidth;
        if(!isNaN(wizardInputWidth)) {
          $("#ui-id-1").css({width: wizardInputWidth});
        }
      },
      source: function(request, response){
        if(recyclingItems.length){
          const results = filterArray(recyclingItems, request.term);
          response(results);
        } else {
          $.ajax({
            url: ajaxUrl,
            dataType: "json",
            data: {
              query: request.term
            },
            success: function(data){
              recyclingItems = tranformData(data);
              const results = filterArray(recyclingItems, request.term);
              response(results);
            }
          }); 
        }
      },
      minLength: 2,
      select: function(event, ui){
        showRecyclingResult(ui.item);
      }
    });

    $("#recycling-search-button").click(function(){
      const term = $("#recycling-wizard").val();
      if(recyclingItems.length){
        const results = filterArray(recyclingItems, term);
        showRecyclingResults(results);
      } else {
        $.ajax({
          url: ajaxUrl,
          dataType: "json",
          success: function(data){
            recyclingItems = tranformData(data); 
            const results = filterArray(recyclingItems, term);
            showRecyclingResults(results);
          }
        });
      }
    });
  }
}

export default disposalSearchModule;