# Disposal Guide

# Getting Started
You need to have npm and git installed in your local computer.
Git clone from project

Go to the project’s (folder name: disposal-guide) location in the command line and run the following command: Npm install

This will install all the dependencies in a node_modules folder inside the disposal-guide folder. Once that’s finished, enter in "npm run build" in the command line. That should create disposal_search.js in the dist folder.

Create a css folder inside the dist folder. You should add jquery-ui.min.css either by downloading the latest jQuery UI file here: https://jqueryui.com/download/all/

Place the index.html and recycling.json files inside the src folder into the dist folder. 

To see index.html enter the following into the command line: npm run dev. You should see the search feature on localhost:8080.

Make your changes to the code in the disposal_search_module.js file in the src folder inside the disposal-guide folder. To test out your changes go to the folder’s location in the command line and run the following command: Npm run dev. This will show you the index.html page on localhost:8080. 

Test out your changes.

Once you are satisfied with your changes, hit CTRL+C on the command line to exit out of npm run dev. Now enter in Npm run build. This will create disposal_search.js file under the dist folder.

# JSON Format
Use disposalSearchModule.init(URL) to feed it the url of the json file that is formatted with the following:
{
  "item": "Name of the item",
  "category": "The category of the recycling item",
  "what_to_do": "How to recycle the item",
  "more_info": "if more text is needed"
} 