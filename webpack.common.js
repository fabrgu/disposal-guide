const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  entry: './src/disposal_search_module.js',
  plugins: [
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['dist/disposal_search.js']})
  ],
  output: {
    filename: 'disposal_search.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'disposalSearchModule',
    libraryExport: 'default',
    libraryTarget: 'var'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  }
};